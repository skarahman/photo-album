﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MongoDB.Bson;
using PhotoAlbum.Domain;
using PhotoAlbum.Services;
using PhotoAlbum.Web.Helpers;
namespace PhotoAlbum.Web.Controllers.API
{
    public class PostController : ApiController
    {
			private readonly PostService _postService;
			private readonly CommentService _commentService;
			public PostController()
			{
				_postService = new PostService();
				_commentService = new CommentService();
			}

        // GET api/post
        public IEnumerable<Domain.Post> Get()
        {
					return _postService.GetPosts();
        }

        // GET api/post/5
				public Domain.Post Get(string id)
        {
					return _postService.GetPost(ObjectId.Parse(id));
        }

        // POST api/post
				public void Post([FromBody]Post post)
        {
					if(ModelState.IsValid)
					{
						post.Url = post.Title.GenerateSlug();
						post.Author = User.Identity.Name;
						post.Date = DateTime.Now;

						_postService.Create(post);
					}

        }

        // PUT api/post/5
				public void Put(ObjectId id, [FromBody]Post post)
        {
					if(ModelState.IsValid)
					{
						post.Url = post.Title.GenerateSlug();

						if(_postService.Edit(post))
						{
						//	return post;
						}

						//ModelState.AddModelError("ConcurrencyError", "This post has been updated since you started editing it. Please reload the post to get the latest changes.");
						//return View(post);
					}
        }

        // DELETE api/post/5
			  [ActionName("Delete")]
        public void Delete(ObjectId id)
        {
					_postService.Delete(id);
        }
    }
}
